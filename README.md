yii2-log
=======
 
Different Yii2 log transports

## Now available

* ElasticsearchTarget
* LogstashFileTarget
* LogstashTarget
* RedisTarget

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

and add next lines to `composer.json`
```json
    "repositories": [
        {
            "url": "https://bitbucket.org/HardTYZ/yii2-log",
            "type": "git"
        }
    ]
```

and add line to require section of `composer.json`

```json
"hardtyz/yii2-log": "*"
```

## Usage

### Common properties

* `$emergencyLogFile`, default `@app/logs/logService.log`

Elasticsearch, Redis and Logstash - are external services, so if they down our logs must be stored in file.
For that **ElasticsearchTarget**, **LogstashTarget**, **RedisTarget** have option `$emergencyLogFile`. It's alias to
file where logs will be written on log service is down.

### ElasticsearchTarget

Stores logs in elasticsearch. All logs transform to json, so you can simply watch them by [kibana](http://www.elasticsearch.org/overview/kibana/).

Extends yii\log\Target, more options see [here](https://github.com/yiisoft/yii2/blob/master/framework/log/Target.php)

##### Example configuration

```php
....
'components' => [
    'log' => [
        'targets' => [
            ['class' => 'hardtyz\\log\\ElasticsearchTarget'],
        ....
```

##### Properties

* `index`, default 'yii' - Elasticsearch index name.
* `type`, default 'log' - Elasticsearch index type.
* `componentName` - Name of yii redis component.

### LogstashFileTarget

Extends yii\log\FileTarget, more options see [here](https://github.com/yiisoft/yii2/blob/master/framework/log/FileTarget.php)

##### Example Yii configuration

```php
....
'components' => [
    'log' => [
        'targets' => [
            ['class' => 'hardtyz\\log\\LogstashFileTarget'],
        ....
```

##### Example Logstash configuration [(current version 1.3.3)](http://logstash.net/docs/1.3.3/)

```
input {
  file {
    type => "yii_log"
    path => [ "path/to/yii/logs/*.log*" ]
    start_position => "end"
    stat_interval => 1
    discover_interval => 30
    codec => "json"
  }
}

filter {
  # ...
}

output {
  stdout => {}
}
```

### LogstashTarget

Extends yii\log\Target, more options see [here](https://github.com/yiisoft/yii2/blob/master/framework/log/Target.php)

##### Example Yii configuration

```php
....
'components' => [
    'log' => [
        'targets' => [
            ['class' => 'hardtyz\\log\\LogstashTarget'],
            // Or UDP.
            [
                'class' => 'hardtyz\\log\\LogstashTarget',
                'dsn' => 'udp://localhost:3333'
            ],
            // Or unix socket file.
            [
                'class' => 'hardtyz\\log\\LogstashTarget',
                'dsn' => 'unix:///path/to/logstash.sock'
            ],
        ....
```

##### Example Logstash configuration [(current version 1.3.3)](http://logstash.net/docs/1.3.3/)

```
input {
  tcp {
    type => "yii_log"
    port => 3333
    codec => "json"
  }
  # Or UDP.
  udp {
    type => "yii_log"
    port => 3333
    codec => "json"
  }
  # Or unix socket file.
  unix {
    type => "yii_log"
    path => "path/to/logstash.sock"
    codec => "json"
  }
}

filter {
  # ...
}

output {
  stdout => {}
}
```


##### Properties

* `dsn`, default `tcp://localhost:3333` - URL to logstash service. Allowed schemas:
    **tcp**, **udp**, **unix** - for unix sock files.
* `index`, default `logstash` - index to logstash service
* `custom`, default `[]` - array custom fields sended to logstash

### RedisTarget

Extends yii\log\Target, more options see [here](https://github.com/yiisoft/yii2/blob/master/framework/log/Target.php)

##### Example Yii configuration

```php
....
'components' => [
    'log' => [
        'targets' => [
            ['class' => 'hardtyz\\log\\RedisTarget'],
        ....
```

##### Example Logstash configuration [(current version 1.3.3)](http://logstash.net/docs/1.3.3/)

```
input {
  redis {
    type => "yii_log"
    key => "yii_log"
    codec => "json"
  }
}

filter {
  # ...
}

output {
  stdout => {}
}
```

##### Properties

* `key`, default `yii_log` - Redis list key.
* `componentName` - Name of yii redis component.

## Testing

#### Run tests from IDE (example for PhpStorm)

- Select Run/Debug Configuration -> Edit Configurations
- Select Add New Configuration -> PHP Script
- Type:
    * File: /path/to/yii-phar/.test.php
    * Arguments run: run  --coverage --html
- OK

#### Run tests from console

```sh
make test
```

-- --

#### Thanks to

[@achretien](https://github.com/achretien)
[@Archy812](https://github.com/Archy812)
[@xt99](https://github.com/xt99)
[@index0h](https://github.com/index0h)

### Fork from

[@index0h](https://github.com/index0h)
[github](https://github.com/index0h/yii2-log/)